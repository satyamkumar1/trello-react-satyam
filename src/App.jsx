import Navbar from './component/Navbar'
import Homepage from './component/Homepage'
import { Route ,Routes} from 'react-router-dom'
import Boardlist from './component/Boardlist'
import Errorpage from './component/Errorpage'

function App() {

  return (
    <>
      <Navbar/>
    <Routes>

     <Route path='/' element={ <Homepage/>}/>
     <Route path='/board/list/:id' element={ <Boardlist/>}/>
      <Route path='*'   element={<Errorpage/>}/>
    </Routes>
    </>
  )
}

export default App
