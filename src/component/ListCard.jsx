import React, { useState, useEffect, memo } from "react";
import { Card, Typography, Button, TextField, Paper, Dialog, DialogActions, DialogTitle, DialogContent, Stack } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import axios from "axios";
import auth from "../assets/config";
import Checklist from "./Checklist";

const ListCard = memo(({ listDataId }) => {
  const [showCard, setShowCard] = useState(true);
  const [cardTitle, setCardTitle] = useState("");
  const [arrcard, setarrcard] = useState([]);

  const config = {
    params: {
      key: auth.key,
      token: auth.token,
    },
  };  
 

// adding card to the list using post api

  const handleAddCard = () => {
    const endpoint = "https://api.trello.com/1/cards";
    const config = {
      name: cardTitle,
      idList: listDataId,
      key: auth.key,
      token: auth.token,
    };

    axios
      .post(endpoint, config)
      .then((response) => {
        setarrcard([...arrcard, response.data]);
      })
      .catch((error) => {
        console.error("Error:", error.response.data);
      });

    setShowCard(true);
    setCardTitle("");
  };


//  getting card form api

function loaddata(){
  const endpoint = `https://api.trello.com/1/lists/${listDataId}/cards`;
   
    axios
      .get(endpoint, config)
      .then((response) => {
        setarrcard(response.data);
      })
      .catch((error) => {
        console.error("Error:", error.response.data);
      });
}
useEffect(() => {
  loaddata();
  }, [listDataId]);


// delete card form api


  function handledeletecard(dataid) {
    const deleteendpoint = `https://api.trello.com/1/cards/${dataid}`;
    axios
      .delete(deleteendpoint, config)
      .then((response) => {
          const newarr = arrcard.filter((data) => {
          return data.id !== dataid;
        });
        setarrcard([...newarr]);
      })
      .catch((error) => {
        console.error("Error deleting card:", error.response.data);
      });
  }

 

  return (
    <>

      <Card variant="outlined" height="auto" key={listDataId}>
        <Typography sx={{ display: "flex", flexDirection: "column", gap: 3 }} variant="h6">
          {arrcard.map((data) => (
            <div  key={data.id} style={{display:"flex"}} > <Paper  elevation={5} sx={{ display: "flex", justifyContent: "space-between", cursor: "pointer" , width:"280px"}}>
                  <Checklist  name={data.name} id={data.id} />
            </Paper>
             <Paper elevation={5}> <CloseIcon style={{ cursor: "pointer",fontSize:"30px"}} onClick={() => handledeletecard(data.id)} /></Paper>
              </div>
              ))}

          {showCard ? (
            <Button size="large" onClick={()=>setShowCard(false)}>
              + add a card
            </Button>
          ) : (
            <div className="add-card1">
              <div className="text-field1">
                <TextField
                  id="outlined-basic"
                  label="Enter title for this card.."
                  variant="outlined"
                  value={cardTitle}
                  onChange={(e) => setCardTitle(e.target.value)}
                />
                <div className="btn">
                  <Button variant="contained" sx={{ width: 110 }} onClick={handleAddCard}>
                    add card
                  </Button>
                  <CloseIcon style={{ cursor: "pointer", fontSize: "40px" }} onClick={() => setShowCard(true)} />
                </div>
              </div>
            </div>
          )}
        </Typography>
      </Card>
    </>
  );
});

export default ListCard;
