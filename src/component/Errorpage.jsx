import React from 'react';
import './Errorpage.css'; // Import custom CSS for styling

const Errorpage = () => {
  return (
    <div className="error-page">
      <h1>Oops! Something went wrong</h1>
      <p>Sorry, we encountered an error while processing your request.</p>
      <p>Please try again later.</p>
    </div>
  );
};

export default Errorpage;
