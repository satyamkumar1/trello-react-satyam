import React, { useState ,useEffect} from "react";
import axios from "axios";
import { Button, Paper, TextField, Typography,Checkbox } from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';
import auth from "../assets/config";
import ProgressBar from "./ProgressBar";

const CheckItem = ({ id,check }) => {
   
  const [show, setShow] = useState(false);
  const [additem, setAdditem] = useState("");
  const [allcheckitem, setAllcheckitem] = useState([]);
  const[count,setcount]=useState(0);
  const[datacount,setDatacount]=useState(0);


// get all checkitem with api

  const loadCheckItems = () => {
    const endpoint = `https://api.trello.com/1/checklists/${id}/checkItems`;

    const config = {
      params: {
        key: auth.key,
        token: auth.token,
      },
    };

    axios
      .get(endpoint, config)
      .then((response) => {

        setAllcheckitem(response.data);
        let count1 = 0;
          response.data.forEach((checkItem) => {
        if (checkItem.state == "complete") {
          count1++;
        }
      });

      setcount(count1);
         setDatacount(allcheckitem.length);
      })
      .catch((error) => {
        console.error("Error fetching checklists:", error.response.data);
      });
  };

  useEffect(() => {
    loadCheckItems();
  }, []);


// add checkitem into the api 

  function handleAdditem() {

    const endpoint = `https://api.trello.com/1/checklists/${id}/checkItems`;
    const config = {      
        name:additem,
        key: auth.key,
        token: auth.token    
    };
   
      axios
            .post(endpoint,config)
            .then((response) => {

              setAllcheckitem([...allcheckitem,response.data])
            })
            .catch((error) => {
              console.error("Error:", error.response.data);
            });
    setShow(false);
  }



// handle delete checkitem from api

function handledeleteitem(itemid){

    const endpoint = `https://api.trello.com/1/checklists/${id}/checkItems/${itemid}`;

    const config = {
      params: {
        key: auth.key,
        token: auth.token,
      },
    };

    axios
      .delete(endpoint, config)
      .then((response) => {

        loadCheckItems();
      })
      .catch((error) => {
        console.error("Error deleted checklists:", error.response.data);
      });
}

// handle checkbox and put into the api

function handleToggleCheck(inputId,checked){
        
               
    const togglecheckendpoint=
        `https://api.trello.com/1/cards/${check.idCard}/checkItem/${inputId}?key=${auth.key}&token=${auth.token}`;
    const config = {
        state: checked ? "complete" : "incomplete",
      };

    axios.put(togglecheckendpoint,config)
        .then((response) => {
            loadCheckItems();
            
          })
          .catch((error) => {

          });


}



  return (
    <>
   <ProgressBar data={datacount} num={count}/>
      {allcheckitem.map((item) => {
        return (
        
          <Paper  key={item.id}  elevation={5} sx={{display:"flex",justifyContent:"space-between",alignItems:"center",margin:"19px"}}>
          <Checkbox checked={item.state !== "incomplete"} onChange={() => handleToggleCheck(item.id, item.state === "incomplete")} />
           <Typography margin={2} padding={1}>{item.name}</Typography> 
           <DeleteIcon sx={{fontSize:"30px",cursor:"pointer"}} onClick={ ()=>handledeleteitem(item.id)}/>
          </Paper>
            

        );
      })}
      {show ? (
              <Paper key={id} sx={{margin:"20px"}}>
                <TextField
                  id="outlined-basic"
                  label="Add an item"
                  variant="outlined"
                  onChange={(e) => setAdditem(e.target.value)}
                />
                <div className="btn">
                  <Button
                    variant="contained"
                    sx={{ width: 50, margin: "10px" }}
                    onClick={handleAdditem}
                  >
                    add
                  </Button>
                  <Button sx={{ width: 50 }} onClick={() => setShow(false)}>
                    cancel
                  </Button>
                </div>
              </Paper>
            ) : (
              <Button
                variant="contained"
                sx={{ margin: "15px" }}
                onClick={()=>setShow(true)}
              >
                Add an item
              </Button>
            )}
    </>
  );
};

export default CheckItem;
