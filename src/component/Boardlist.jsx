  import React, { useState, useEffect,memo,useMemo } from "react";
  import { Box, Button, Container, Paper, Stack ,TextField, Typography,Dialog, DialogTitle, DialogActions} from "@mui/material";
  import axios from "axios";
  import { useParams } from "react-router-dom";
  import ListCard from "./ListCard"; 
  import CloseIcon from '@mui/icons-material/Close';
  import auth from "../assets/config";
  import "./Boardlist.css";
  import Errorpage from "./Errorpage";

  const Boardlist = () => {
    const [arr, setarr] = useState([]);
    const [listname, setlistname] = useState("");
    const [show, setshow] = useState(false);
    const[open,setOpen]=useState(false);
    const [deletedId,setDeleteid]=useState('');
    const[handlerror,setHandleerror]=useState(false);


    const id = useParams().id;
    const listData = useMemo(() => arr, [arr]);

//getting list on board page


    const endpoint = `https://api.trello.com/1/boards/${id}/lists`;
    const config = {
      params: {
        key: auth.key,
        token: auth.token,
      },
    };

    useEffect(() => {
      axios
        .get(endpoint, config)
        .then((response) => {
          setarr(response.data);
        })
        .catch((error) => {
          setHandleerror(true);
          
        });
    }, []);

    function handleTogglebtn() {
      setshow(true);
    }


// posting list name through api


    function handleTogglebtn2() {
      const endpoint = "https://api.trello.com/1/lists";
      const config = {
        name: listname,
        idBoard: id,
        key: auth.key,
        token: auth.token,
      };

      axios
        .post(endpoint, config)
        .then((response) => {
          setarr([...arr, response.data]);
        })
        .catch((error) => {
          setHandleerror(true);

        });

      setshow(false);
    }


//  deleting list name thorugh api

    
      function handledeletelist(listid){
       setDeleteid(listid);
          setOpen(true);
      }
      function handleclose(){
        setOpen(false);
      }
      function deleteListFromDialog(){
        const deleteEndpoint=`https://api.trello.com/1/lists/${deletedId}`;
        const closevalue={
          closed:true
        }
          axios
          .put(deleteEndpoint,closevalue ,config)
          .then((response) => {
            const newarr=arr.filter((item)=>{
              return item.id!==response.data.id;
            })
            setarr([...newarr]);
          
          })
          .catch((error) => {
          setHandleerror(true);

          });
          setOpen(false);
      }



    return (
      <>
      {handlerror?<Errorpage/>:" "}
      <Dialog open={open} onClose={handleclose}>
    <DialogTitle>Are you sure want to delete!</DialogTitle>
    <DialogActions>
      <Button color="error" variant="contained" onClick={()=>{deleteListFromDialog()}}>Delete</Button>
    </DialogActions>
       </Dialog>
            
       <Container maxWidth="auto" style={{ overflowX: "scroll", height: "100vh" }}>
        <Container className="container"
          maxWidth="100%"
            style={{
            display: "flex",
             gap: "2rem",
            marginTop: "5%",
            flexWrap:"nowrap"
          }}
        >
          {listData.map((data) => (
            <Box key={data.id}>
              <Paper sx={{ display: "flex", gap: 3 }} elevation={10}>
              <Stack sx={{ width: "300px"}} spacing={4}>
                <div key={data.name} className="board-head">{data.name}
                <CloseIcon style={{ cursor: "pointer" , backgroundColor:"red",marginTop:"5px",color:"white"}} onClick={()=>handledeletelist(data.id)}/>
                </div>
                <ListCard listDataId={data.id} />
              </Stack>
              </Paper>
            </Box>
          ))}

          <div className="add-card">
            {show ? (
              <div className="text-field">
                <TextField
                  id="outlined-basic"
                  label="Enter list title.."
                  variant="outlined"
                  onChange={(e) => setlistname(e.target.value)}
                />
                <div className="btn">
                  <Button
                    variant="contained"
                    sx={{ width: 100 }}
                    onClick={handleTogglebtn2}
                  >
                    add list
                  </Button>
                  <CloseIcon
                    onClick={() => setshow(false)}
                    style={{ cursor: "pointer" ,fontSize:"30px"}}
                  />
                </div>
              </div>
            ) : (
              <Button variant="contained" sx={{ width: "200px" }} onClick={handleTogglebtn}>
                + Add Another list
              </Button>
            )}
          </div>
        </Container>
        </Container>
  
            
      </>
    );
  };

  export default memo(Boardlist);
