import React, { useState, useEffect } from "react";
import { Dialog, Paper,DialogActions, DialogContent, DialogTitle, Button, Stack, Typography, TextField ,Slider, Grid} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import axios from "axios";
import auth from "../assets/config";
import CheckItem from "./CheckItem";


const Checklist = ({ name, id }) => {
  const [openAddchecklist, setOpenAddchecklist] = useState(false);
  const [checklistTitle, setChecklistTitle] = useState("");
  const [checklists, setChecklists] = useState([]);
  const [isOpen, setIsOpen] = useState(false);

 
// create checklist  

  const handleCreateChecklist = () => {
    const endpoint = "https://api.trello.com/1/checklists";
    const config = {
      name: checklistTitle,
      idCard: id,
      key: auth.key,
      token: auth.token,
    };

    axios
      .post(endpoint, config)
      .then((response) => {
        
            setChecklistTitle(""); 
            setChecklists([...checklists,response.data]);
            })
      .catch((error) => {
        console.error("Error creating checklist:", error.response.data);
      });

    setOpenAddchecklist(false);
  }

// get all checklist


  const loadChecklists = () => {
    const endpoint = `https://api.trello.com/1/cards/${id}/checklists`;
    const config = {
      params: {
        key: auth.key,
        token: auth.token,
      },
    };

    axios
      .get(endpoint, config)
      .then((response) => {
        
        setChecklists(response.data);
      })
      .catch((error) => {
        console.error("Error fetching checklists:", error.response.data);
      });
  };

  // useEffect(() => {
  //   loadChecklists();
  // }, []);


//delete checklist


  const handleDeleteChecklist = (checklistId) => {  
   
    const endpoint = `https://api.trello.com/1/checklists/${checklistId}`;
    const config = {
      params: {
        key: auth.key,
        token: auth.token,
      },
    };
 
    axios
      .delete(endpoint,config)
      .then((response) => {

        const newarr=checklists.filter((item)=>{
          return item.id!==checklistId;
        })
        setChecklists([...newarr]);
      })
      .catch((error) => {
        console.error("Error deleting checklist:", error.response);
      });

    }



  return (
    <>
   <Paper  onClick={()=>{setIsOpen(true); loadChecklists()}} sx={{width:"264px"}} > <div style={{margin:5}}>{name}</div></Paper>
      <Dialog open={openAddchecklist} onClose={()=>setOpenAddchecklist(false)}>
        <DialogTitle>
          Add checklist
          <CloseIcon style={{ cursor: "pointer", fontSize: "30px", float: "right" }} onClick={()=>setOpenAddchecklist(false)}/>
        </DialogTitle>
        <DialogContent sx={{ height: "auto" }}>
          <Stack spacing={2} margin={2}>
            <Typography> Title</Typography>
            <TextField variant="outlined" label="checklist" value={checklistTitle} onChange={(e) => setChecklistTitle(e.target.value)} />
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={handleCreateChecklist}>
            add
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog open={isOpen} onClose={()=>setIsOpen(false)}>
        <DialogTitle sx={{fontWeight:"bold",fontSize:"40px"}}>{name}
        <CloseIcon style={{ cursor: "pointer", fontSize: "30px", float: "right" }} onClick={()=>setIsOpen(false)} />
        
        </DialogTitle>
        <DialogContent sx={{ height: "500px", width:"500px" }}>
          {checklists.map((checklist) => (
          <Paper   key={checklist.id} elevation={10} margin={2} sx={{width:"400px",height:"auto",marginTop:"30px"}} > 
          <div style={{display:"flex",gap:'10rem'}}>
            <Typography key={checklist.id} margin={2} sx={{fontWeight:"bold",fontSize:"20px"}}>{checklist.name}</Typography>
          <Button onClick={()=>handleDeleteChecklist(checklist.id)}>delete</Button>
          </div>
        
            <CheckItem id={checklist.id} check={checklist} key={checklist.id}/>
            
          </Paper> 
          ))}
        </DialogContent>
        <DialogActions>
          <Button color="success" variant="contained" onClick={()=>setOpenAddchecklist(true)}>
            Add Checklist
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default Checklist;
