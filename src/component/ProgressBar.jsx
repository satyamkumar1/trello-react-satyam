import * as React from 'react';
import PropTypes from 'prop-types';
import LinearProgress from '@mui/material/LinearProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

function LinearProgressWithLabel(props) {

  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
          <Box sx={{ minWidth: 35,marginLeft:2 }}>
        <Typography variant="body2" color="text.secondary">{`${Math.round(
          props.value,
        )}%`}</Typography>
      </Box>
      <Box sx={{ width: '90%', mr: 1 }}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
    
    </Box>
  );
}

LinearProgressWithLabel.propTypes = {

  value: PropTypes.number.isRequired,
};

export default function LinearWithValueLabel({data,num}) {
  

  return (
    <Box sx={{ width: '100%' }}>
      <LinearProgressWithLabel value={data!=0?num*100/data:0} />
    </Box>
  );
}
