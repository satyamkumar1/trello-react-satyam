import React, { useEffect, useState } from "react";
import { Button, Container, Paper, Typography, Box } from "@mui/material";
import axios from "axios";
import { Link } from "react-router-dom";
import auth from "../assets/config";
import Loader from "./Loader";
import Errorpage from "./Errorpage";

const Homepage = () => {  

  const[handleerror,setHandleerror]=useState(false);
  const [loading,setLoading]=useState(true);
  const [showcard, setshowcard] = useState(false);
  const [boardname, setboardname] = useState("");
  const [arr, setarr] = useState([]);



  const config = {
    params: {
      key: auth.key,
      token: auth.token,
    },
  };

// creating  a board and using post api


  function createBoard() {
    if (boardname.length > 0) {
      const endpoint = "https://api.trello.com/1/boards/";

      const postData = {
        name: boardname,
      };

      axios
        .post(endpoint, postData, config)
        .then((response) => {
          setarr([...arr, response.data]);
        })
        .catch((error) => {
          setHandleerror(true);
        });

      setshowcard(false);
    }
  }


// get all board name form api using get 


  useEffect(() => {

  const endpointGetAllBoard  = "https://api.trello.com/1/members/me/boards";

    axios
      .get(endpointGetAllBoard, config)
      .then((response) => {
        setLoading(false);
        setarr(response.data);
      })
      .catch((error) => {
        setHandleerror(true);       
       
      });
  }, []);


  

  return (
    <>
  {handleerror?<Errorpage/>:" "}
    {loading?<Loader/>:
      <Container
        maxWidth="100%"
        style={{
          display: "flex",
          gap: "2rem",
          marginTop: "5%",
          flexWrap: "wrap",
        }}
      >
        {arr.map((item) => {
          return (
            <Link
              to={`/board/list/${item.id}`}
              style={{ textDecoration: "none" }}
              key={item.id}
            >
              <Paper
                style={{
                  display: "flex",
                  color: "",
                  width: "300px",
                  height: "200px",
                  alignItems: "center",
                  justifyContent: "center",
                  backgroundImage: `url(${item.prefs.backgroundImage})`,
                  backgroundSize: "cover",
                }}
                elevation={10}
              >
                <div>
                  <Typography variant="h3"> {item.name}</Typography>
                </div>
              </Paper>
            </Link>
          );
        })}

        <Paper
          elevation={20}
          style={{
            display: "flex",
            color: "",
            width: "300px",
            height: "200px",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Button variant="contained" onClick={()=>setshowcard(true)}>
            create new Board
          </Button>
        </Paper>

        {showcard ? (
          <>
            <Paper
              style={{
                display: "flex",
                gap: "1rem",
                flexDirection: "column",
                width: "300px",
                height: "200px",
                alignItems: "center",
                justifyContent: "center",
                backgroundSize: "cover",
                fontSize:20
              }}
              elevation={10}
            >
              <label>Board title:</label>
              <input
                type="text"
                onChange={(e) => setboardname(e.target.value)} style={{padding:10}}
              />
              <Button variant="contained" onClick={createBoard}>
                submit
              </Button>
            </Paper>
          </>
        ) : (
          " "
        )}
      </Container>
} 
    </>
  );
};

export default Homepage;
